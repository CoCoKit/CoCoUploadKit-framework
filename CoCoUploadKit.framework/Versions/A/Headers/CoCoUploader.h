//
//  CoCoUploader.h
//  CoCoUploadKit
//
//  Created by 陈明 on 2018/4/26.
//  Copyright © 2018年 陈明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoCoUploadTask.h"

@interface CoCoUploaderConfig : NSObject

@property (nonatomic, assign) NSUInteger maxConcurrent;// 最大并发数，默认为2

- (instancetype)initWithConcurrent:(NSUInteger)concurrent;
@end

@interface CoCoUploader : NSObject
@property (nonatomic, assign, readonly) CoCoUploadStatus uploadStatus;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, assign) CGFloat uploadProgress;


// 向容器中添加一个资源
- (BOOL)addUploadTask:(CoCoUploadTask *)task;

- (BOOL)addUploadTasks:(NSArray <CoCoUploadTask *> *)tasks;

// 开始上传
- (BOOL)beginUpload;

// 放弃、停止上传
- (void)dropUpload;

- (NSArray<CoCoUploadTask *> *)successedTasks;
- (NSArray<CoCoUploadTask *> *)failureTasks;

- (instancetype)initWithConfig:(CoCoUploaderConfig *)config;

- (instancetype)init __attribute__((unavailable("禁止使用init方法，请使用initWithConfig:")));
@end
