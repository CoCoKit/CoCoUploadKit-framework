//
//  CoCoUploadMacros.h
//  CoCoUploadKit
//
//  Created by 陈明 on 2018/4/27.
//  Copyright © 2018年 陈明. All rights reserved.
//

#ifndef CoCoUploadMacros_h
#define CoCoUploadMacros_h


/**
 * 下载任务的下载状态
 */
typedef enum : NSUInteger {
    CoCoUploadStatusIdle,
    CoCoUploadStatusUploading,
    CoCoUploadStatusSuccessed,
    CoCoUploadStatusFailed,
    CoCoUploadStatusCanceld,
} CoCoUploadStatus;



/**
 * 单个条目的下载状态
 */
typedef enum : NSUInteger {
    CoCoUploadItemStatusIdle,
    CoCoUploadItemStatusUploading,
    CoCoUploadItemStatusSuccessed,
    CoCoUploadItemStatusFailed,
    CoCoUploadItemStatusCanceld,
    CoCoUploadItemStatusReady,
} CoCoUploadItemStatus;


/**
 * 初始化的数据类型
 */
typedef enum : NSUInteger {
    CoCoUploadItemSourceTypePHAsset,
    CoCoUploadItemSourceTypeImage,
    CoCoUploadItemSourceTypeNSData,
    CoCoUploadItemSourceTypeFilePath,
} CoCoUploadItemSourceType;


#endif /* CoCoUploadMacros_h */
