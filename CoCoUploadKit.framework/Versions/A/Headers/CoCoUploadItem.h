//
//  CoCoUploadItem.h
//  CoCoUploadKit
//
//  Created by 陈明 on 2018/4/26.
//  Copyright © 2018年 陈明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CoCoUploadMacros.h"

@class PHAsset;

@interface CoCoUploadItem : NSObject
@property (nonatomic, assign, readonly) CoCoUploadItemSourceType sourceType;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *mime;// 文件类型
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) id customObject;
@property (nonatomic, assign) CGFloat quility;// 默认为1

- (instancetype)initWithPHAsset:(PHAsset *)asset;
- (instancetype)initWithImage:(UIImage *)image;
- (instancetype)initWithData:(NSData *)data;
- (instancetype)initWithFilePath:(NSString *)filePath;
- (NSData *)fileData;


- (instancetype)init __attribute__((unavailable("禁止使用init方法，请使用initWithXXX方法")));
@end
