//
//  CoCoUploadKit.h
//  CoCoUploadKit
//
//  Created by 陈明 on 2018/4/26.
//  Copyright © 2018年 陈明. All rights reserved.
//

#import <UIKit/UIKit.h>

// ! Project version number for CoCoUploadKit.
FOUNDATION_EXPORT double CoCoUploadKitVersionNumber;

// ! Project version string for CoCoUploadKit.
FOUNDATION_EXPORT const unsigned char CoCoUploadKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoCoUploadKit/PublicHeader.h>

#import <CoCoUploadKit/CoCoUploader.h>
#import <CoCoUploadKit/CoCoUploadItem.h>
#import <CoCoUploadKit/CoCoUploadTask.h>
