//
//  CoCoUploadTask.h
//  CoCoUploadKit
//
//  Created by 陈明 on 2018/4/27.
//  Copyright © 2018年 陈明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoCoUploadItem.h"
#import "CoCoUploadMacros.h"

@class CoCoUploaderConfig, AFHTTPSessionManager;

@interface CoCoUploadTask : NSObject

@property (nonatomic, strong, readonly) CoCoUploadItem *item;
@property (nonatomic, assign, readonly) CGFloat uploadProgress;
@property (nonatomic, strong) NSMutableDictionary *parames;
@property (nonatomic, assign) CoCoUploadItemStatus taskStatus;
@property (nonatomic, strong) NSString *uploadServerUrl;
@property (nonatomic, strong) NSString *responseStatusPath;// @"rsp.code"
@property (nonatomic, strong) NSString *responseMessage;
@property (nonatomic, assign) CGFloat timeOut;// 默认80秒
@property (nonatomic, assign) NSInteger responseSuccessValue;// 0
@property (nonatomic, strong) NSDictionary *response;
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;
@property (nonatomic, strong) NSURLSessionDataTask *task;

- (instancetype)initWithItem:(CoCoUploadItem *)item;

- (instancetype)init __attribute__((unavailable("禁止使用init方法，请使用initWithItem:")));

- (void)uploadWithConfig:(CoCoUploaderConfig *)config complete:(void (^)(CoCoUploadItemStatus taskStatus, NSError *error))completeBlock;
- (void)updateProgress:(CGFloat)progress;
- (void)cancle;
@end
